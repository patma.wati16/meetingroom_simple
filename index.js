//use path module
const path = require('path');
//use express module
const express = require('express');
//use hbs view engine
const hbs = require('hbs');
//use bodyParser middleware
const bodyParser = require('body-parser');
//use mysql database
const mysql = require('mysql');
const app = express();



//Create Connection
const conn = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '',
  port :3306,
  database: 'meetingroom'
});

//connect to database
conn.getConnection((err) =>{
  if(err) throw err;
  console.log('Mysql Connected...');
});

//set views file
app.set('views',path.join(__dirname,'views'));
//set view engine
app.set('view engine', 'hbs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//set folder public as static folder for static file
app.use('/assets',express.static(__dirname + '/public'));



//route for homepage
app.get('/',(req, res) => {
  let sql = "SELECT * FROM ruangan";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.render('meetingroom_list',{
      results: results
    });
  });
});


//route for pesan ruang meeting
app.get('/pesanroom',(req, res) => {
  let sql = "SELECT * FROM ruangan";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.render('pesanroom_list',{
      results: results
    });
  });
});


app.get('/pesan',(req, res) => {
  let sql = "SELECT * FROM pesanan";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.render('pesan',{
      results: results
    });
  });
});



//route for insert data
app.post('/save',(req, res) => {
  let data = {nama: req.body.nama,  daerah: req.body.daerah, kapasitas: req.body.kapasitas, harga: req.body.harga,};
  let sql = "INSERT INTO ruangan SET ?";
  let query = conn.query(sql, data,(err, results) => {
    if(err) throw err;
    res.redirect('/');
    //res.json({results});
  });
});

//route for insert pesanan
app.post('/pesanroom/create',(req, res) => {
  let data = {idruang: req.body.id,  email: req.body.email, durasi: req.body.durasi};
  let sql = "INSERT INTO pesanan SET ?";
  let query = conn.query(sql, data,(err, results) => {
    if(err) throw err;
    res.redirect('/pesan');
    //res.json({results});
  });
});


//server listening
app.listen(7000, () => {
  console.log('Server is running at port 7000');
});



